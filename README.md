Assignment:1

1)Install docker-compose on your machine, if not already installed.

`apt-get install docker-compose`

2)Check docker-compose version.

`docker-compose -v`

```
docker-compose version 1.29.2, build 5becea4c
```

3)Create a directory named nginx in your root.
	
`mkdir nginx`

4)Switch to that directory and create a file named docker-compose.yaml
`cd nginx`

`touch docker-compose.yaml`

5)Use docker-compose version 3 to create docker-compose.yaml file.
```
version: "3.3"
```
6)Create a service named "databases".
```
version: "3.3"
services:
 	databases:
```
7)Use image named "mysql"
```
version: "3.8"
services:
    databases:
	    image: mysql
```
8)Map container 3306 port to host machine 3306 port.
```
version: "3.8"
services:
	databases:
		image: mysql	
		ports:
			- "3306:3306"
```
9)Add environment variables named "MYSQL_ROOT_PASSWORD", "MYSQL_DATABASE", "MYSQL_USER" and "MYSQL_PASSWORD" along with corresponding values for all.
```
environment:
   - "MYSQL_ROOT_PASSWORD=yes"
   - "MYSQL_DATABASE=test"
   - "MYSQL_USER=demo"
   - "MYSQL_PASSWORD=demo"
```
10)Add another service named "web"   Use image "nginx"
```
    web:     
   	    image: nginx
```
12)Save docker-compose.yaml file and do docker-compose up.

`docker-compose up`
```
	Starting nginx_databases_1 ... done
	Starting nginx_web_1       ... done
```
13)Verify nginx service is up and is accessible on machine.
	Checked in UI
14)Stop and remove your docker container using docker-compose.

`docker-compose stop`
```
Stopping nginx_web_1       ... done
Stopping nginx_databases_1 ... done
```
`docker-compose rm`
        
```
	Going to remove nginx_web_1, nginx_databases_1
	Are you sure? [yN] y
	Removing nginx_web_1       ... done
	Removing nginx_databases_1 ... done	
```

**Assignment:2**

1)Create a directory named wordpress on your local.

`mkdir wordpress`

2)switch to that directory and create a file named docker-compose.yaml

`cd wordpress`

`touch docker-compose.yaml`

3)Use docker-compose version 3 to create docker-compose.yaml file.
```
version: '3'
```
4)Create a service named "wordpress" using wordpress:latest image.
```
version: '3'
services:
	wordpress:
		image: wordpress:latest
```
5)Map port of wordpress container port 80 to host system port 8000.
```
ports:
	- "8000:80"
```
6)Add a parameter to restart container in case service went down.
```
restart: on-failure
```
7)Within wordpress environment variable, add wordpress_db_host value along with port.
```
	environment:
  	  - "WORDPRESS_DB_HOST=db:3306"
```
8)Also add one more variable named wordpress_db_password.
```
   - "WORDPRESS_DB_PASSWORD=demo"
```
9)Add a volume named wordpress_data.
```
	version: '3.3'
	volume:
		wordpress_data:
```
10)Add one more service named "db" under same docker-compose file.
```
version: '3'
services:
	wordpress:
	.
	.
	.
	db:
```
11)Use image named mysql with version 5.7
```
db:
	image: mysql:5.7	
```
12)Use volume wordpress_data and map it to /var/lib/mysql
```
    volumes:
 	    - wordpress_data:/var/lib/mysql
```
13)Enable always restart parameter.
	
`restart: always`

14)Add environment variables named "MYSQL_ROOT_PASSWORD", "MYSQL_DATABASE", "MYSQL_USER" and "MYSQL_PASSWORD" along with corresponding values for all.
```
	environment:
        - "MYSQL_ROOT_PASSWORD=demo"
        - "MYSQL_DATABASE=test"
        - "MYSQL_USER=user"
        - "MYSQL_PASSWORD=pass”
```
15)At last add a dependency of db service in wordpress service.
```
	depends_on:
 	  - db
```
16)save the file and build docker-compose.yaml and create containers for wordpress and db machine.

`docker-compose up -d`

17)Display currently bulit containers created using docker-compose.

`docker-compose ps`

|		Name           |            Command             |  State |         Ports         |
|----------------------|--------------------------------|--------|-----------------------|
|wordpress_db_1        |  docker-entrypoint.sh mysqld   |   Up   |   3306/tcp, 33060/tcp |
|wordpress_wordpress_1 |  docker-entrypoint.sh apach ...|   Up   |  0.0.0.0:8000->80/tcp |

18)Stop and remove your docker container using docker-compose.

`docker-compose stop `
```
Stopping wordpress_wordpress_1 ... done
Stopping wordpress_db_1        ... done
```
`docker-compose rm`
```
Going to remove wordpress_wordpress_1, wordpress_db_1
Are you sure? [yN] y
Removing wordpress_wordpress_1 ... done
Removing wordpress_db_1        ... done
```
